---
layout: markdown_page
title: "Category Vision - Review Apps"
---

- TOC
{:toc}

## Review Apps

Review Apps let you build reviews right into your software development workflow by automatically provisioning test environments for your code, integrated right into your merge requests.

This area of the product is in need of continued refinement to add more kinds of review apps (such as for mobile devices), and a smoother, easier to use experience.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=review%20apps&sort=milestone)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Reach out to PM Jason Lenny ([E-Mail](mailto:jason@gitlab.com) / 
[Twitter](https://twitter.com/j4lenn)) if you'd like to provide feedback or ask 
questions about what's coming.

## What's Next & Why

Next up is [gitlab-ce#42822](https://gitlab.com/gitlab-org/gitlab-ce/issues/42822), our top CS item (as well as a popular customer item) which helps manage your review apps and keep the number/resources used from growing out of control in large projects. This was chosen because it can quickly become a maintenance problem, and solving this soon will make Review Apps much easier for administrators to deal with.

## Competitive Landscape

### Heroku

One big advantage Heroku Review Apps have over ours is that they are easier to set up and get running. Ours require a bit more knowledge and reading of documentation to make this clear. We can make our Review Apps much easier (and thereby much more visible) by implementing [gitlab-ce#27893](https://gitlab.com/gitlab-org/gitlab-ce/issues/27893), which does the heavy lifting of getting them working for you.

## Analyst Landscape

We do not engage with analysts in the review apps space, so this is N/A.

## Top Customer Success/Sales Issue(s)

Management of Review Apps can be a challenge, particularly in cleaning them up. To highlight the severity of how this issue can grow, www-gitlab-com project has over 1,500 running stale environments at the time of writing this, with no clear easy way to clean them up. There are two main items that look to address this challenge:

- [gitlab-ce#42822](https://gitlab.com/gitlab-org/gitlab-ce/issues/42822) builds in an expiration date for review apps, beyond which they will automatically be terminated.
- [gitlab-ce#38718](https://gitlab.com/gitlab-org/gitlab-ce/issues/38718) (also the #2 customer issue) implements a way to clean up environments that either did not have an expiration date or were not terminated for other reasons.

The first issue does not help instances that already have the problem, but we will go after it first as it stops the issue from becoming worse. After that, we will look at the need for the second (which may be able to be built on top of the first.)

Note that this was selected as a top CS/Sales issue based on anecdotal conversations. If the CS/Sales teams have additional feedback it is of course welcome.

## Top Customer Issue(s)

The top customer issue impacting users of Review Apps is [gitlab-ce#27424](https://gitlab.com/gitlab-org/gitlab-ce/issues/27424), which enables truly dynamic URLs, giving additional flexibility for teams to name their review apps.

## Top Internal Customer Issue(s)

TBD

## Top Vision Item(s)

Our focus for the vision is to bring Review Apps to mobile workflows via [gitlab-ce#40683](https://gitlab.com/gitlab-org/gitlab-ce/issues/40683) - adding support to Android/iOS emulators via the Review App will enable a whole new kind of development workflow in our product, and make Review Apps even more valuable.
