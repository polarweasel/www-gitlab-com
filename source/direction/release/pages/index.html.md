---
layout: markdown_page
title: "Category Vision - Pages"
---

- TOC
{:toc}

## Pages

Pages allows you to automatically document and deliver your content through your delivery pipelines with built-in content hosting support.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=pages&sort=milestone)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Reach out to PM Jason Lenny ([E-Mail](mailto:jason@gitlab.com) / 
[Twitter](https://twitter.com/j4lenn)) if you'd like to provide feedback or ask 
questions about what's coming.

### Overall Prioritization

Pages is not our most overall strategic feature, but it's a well-loved feature and one that people really enjoy engaging with as part of the GitLab experience. We do not expect to provide a market-leading solution in this space for hosting all kinds of static websites, but one which serves the purpose of publishing static web content that goes with your releases.

## What's Next & Why

Our next target is [gitlab-ee#30548](https://gitlab.com/gitlab-org/gitlab-ce/issues/30548) (adding support to subgroups) because it is the #1 CS focus item as well as our #2 customer issue, and represents an area that users are clearly passionate about and interested in.

## Competitive Landscape

### GitHub Pages

[gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996) (automatic certificate renewal) has been highlighted by users as a pain point, and since GitHub handles this automatically, it drives users who are particularly annoyed by this gap to GitLab. By resolving this issue we would remove one good reason why people would move to GitHub over GitLab.

### Netlify

TBD - reference material available at [gitlab-ce#48006](https://gitlab.com/gitlab-org/gitlab-ce/issues/48006).

## Analyst Landscape

We do not engage with analysts in the Pages space, so this is N/A.

## Top Customer Success/Sales Issue(s)

The top CS item is [gitlab-ce#30548](https://gitlab.com/gitlab-org/gitlab-ce/issues/30548). Adding support for subgroups will expand Pages to include even more use cases, giving more kinds of users access to this great feature. It's a clearly valuable use case that is worth delivering.

## Top Customer Issue(s)

The most popular customer issue is [gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996), which makes setting up and renewing certificates automatic for your Pages site. This is an ongoing hassle once you set up your Pages site since renewals in particular are frequent and manual. Solving this problem will make Pages even more automatic and easy to use.

## Top Internal Customer Issue(s)

TBD

## Top Vision Item(s)

From a vision standpoint, adding Review Apps for Pages ([gitlab-ce#26621](https://gitlab.com/gitlab-org/gitlab-ce/issues/26621)) is interesting because it allows for more sophisticated development flows involving testing, where at the moment the only environment that GitLab understands is production. This would level up our ability for Pages to be a more mission-critical part of projects and groups.
