---
layout: markdown_page
title: "Product Vision - Release"
---

- TOC
{:toc}

## Release Automation & Orchestration

The Release stage of the DevOps pipeline is focused on enabling zero-touch delivery of
your software - taking the output of your builds and safely, automatically, and 
reliably moving your software through your path to production with no additional 
configuration. The stage aligns to two main industry categories: [Continuous Delivery and Release Automation](https://go.forrester.com/blogs/continuous-delivery-and-release-automation-the-missing-link-for-business-transformation/) 
and [Application Release Automation/Orchestration](https://en.wikipedia.org/wiki/Application-release_automation). Check
out the video below for a brief walkthrough of this page and what's coming.

<figure class="video_container">
<iframe src="https://www.youtube.com/embed/Xw1PSEmtVwE" frameborder="0" allowfullscreen="true" width="320" height="180"> </iframe>
</figure>

Reach out to PM Jason Lenny ([E-Mail](mailto:jason@gitlab.com) / 
[Twitter](https://twitter.com/j4lenn)) if you'd like to provide feedback or ask 
questions about what's coming.

## Contributing

At GitLab, one of our values is that everyone can contribute. If you're looking to
get involved with features in the Release area, there are a couple queries you can use
to find issues to work on:

- [UI Polish](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Accepting%20merge%20requests&label_name[]=devops%3Arelease) items are generally frontend development (or at least minimal backend), are very self-contained, and are great, very visible items that can make a big difference when it comes to usability.
- [Accepting Merge Request](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=UI+polish&label_name%5B%5D=devops%3Arelease&scope=all&state=opened) items are open to contribution. These are generally a bit more complicated, so if you're interested in contributing we recommend you open up a dialog with us in the issue.
- [Community Contribution](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20contribution&label_name[]=devops%3Arelease) items may already have some contributors working on them if you want to join up. There aren't always issues here to find.

You can read more about our general contribution guidelines [here](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md).

## North Stars

There are a few important ideas we are keeping as northern stars to guide us forward 
in this space. With each of these, we're focusing on more complete (truly minimally 
lovable) features. Iterations are how we build software, but at the same time we 
want to continue to curate those features that have proven their value, allowing 
them to grow into more complete, lovable features that exceed expectations.

You can see an calendar view of the epics below at [this link](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Arelease&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).

##### Zero-Touch Delivery

Technologies like Heroku/PAS Buildpacks are leading the way in allowing for
developers to focus on building code and letting the automation handle figuring out
how to manage the deployment. Our vision is for CD to take over from your build
system and predictably manage your delivery for you while remaining completely
customizable to support any unique cases.

Related Epics:

- [Make CD more reliable and require less interaction](https://gitlab.com/groups/gitlab-org/-/epics/770)
- [Better management of stability in master](https://gitlab.com/groups/gitlab-org/-/epics/101)
- [CI/CD Reporting & Analytics improvements](https://gitlab.com/groups/gitlab-org/-/epics/358)
- [Make Feature Flags viable](https://gitlab.com/groups/gitlab-org/-/epics/763)
- [A/B/n testing using Feature Flags](https://gitlab.com/groups/gitlab-org/-/epics/712)

##### On-Demand Environments

Delivering software to your environments automatically is the first step, but 
new technologies like Kubernetes are now allowing on-demand environments to be
created in parallel with code deployments like never before. By enabling this 
on-demand infrastructure to understand the intentions of developers, you can 
enable a true one-click "deliver to my users" workflow.

Related Epics:

- [Make environments easier to use and manage](https://gitlab.com/groups/gitlab-org/-/epics/767)
- [Improve certificate handling for Pages](https://gitlab.com/groups/gitlab-org/-/epics/766)

##### Secure, Compliant, and Well-Orchestrated Deployments

Releasing in GitLab takes advantage of our [deeply connected ecosystem](/handbook/product/#single-application) 
to provide auditability all the way from planning through monitoring changes. 
Compliance and security evidence is gathered and validated at each step of 
the way, giving you confidence in what you're delivering in your release. 
Deployments are run from outside the build pipeline, ensuring a clear boundary 
between your developers and changes to your environments inside of a well-orchestrated
and automated delivery process.

Related Epics:

- [Lock down path to production](https://gitlab.com/groups/gitlab-org/-/epics/762)
- [Cross-project pipeline triggering and visualization](https://gitlab.com/groups/gitlab-org/-/epics/414)
- [Mature release orchestration](https://gitlab.com/groups/gitlab-org/-/epics/771)
- [Improve mobile releasing using GitLab](https://gitlab.com/groups/gitlab-org/-/epics/769)

<%= partial("direction/categories", :locals => { :stageKey => "release" }) %>

## What's Next

It's important to call out that the below plan can change any moment and should not be 
taken as a hard commitment, though we do try to keep things generally stable. In 
general, we follow the same [prioritization guidelines](/handbook/product/#prioritization) 
as the product team at large. Issues will tend to flow from having no milestone, to 
being added to the backlog, to being added to this page and/or a specific milestone 
for delivery.

<%= direction %>

<%= partial("direction/other", :locals => { :stage => "release" }) %>
