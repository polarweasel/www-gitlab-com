---
layout: markdown_page
title: "Incident Management"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Incidents

Incidents are **anomalous conditions** that result in **service degradation** or **outages** and require human or automated intervention to restore service to full operational status in the shortest amount of time possible. 

The primary goal of incident management is to organize chaos into swift incident resolution. To that end, incident management provides:

* incident **severity** classification to assess impact and scope,
* well-defined **roles** for members of the incident team, 
* control points to manage the flow of both **resolution path** and **information**, 
* and **root-cause analysis** and introspective analysis procedures.

## Severities

**Incident severities encapsulate the impact of an incident and scope the resources allocated to handle it**. Detailed definitions are provided for each severity, and these definitions are reevaluated as new circumstances become known. Incident management uses our standarized severity definitions, which can be found under our [issue workflow documentation](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#severity-labels).

* `S1` and `S2` incidents cannot be considered closed until GitLab.com has been fully operational, onine, stable and performant for 30 minutes after the incident was resolved.
* When a closed `S2` or `S3` incident is followed by another `S2` or `S3` incident within 3 hours, the latter incidents are automatically upgraded to `S2` incidents.

### Alert Severities

* Alerts severities do not necessarily determine incident severities. A single incident can trigger a number of alerts at various severities, but the determination of the incident's severity is driven by the above definitions.
* Over time, we aim to automate the determination of an incident's severity through service-level monitoring that can aggregate individual alerts against specific SLOs.

## Roles

| **Role** | **Definition and Examples** |
| -------- | ------------------------|
| `EOC+IT` | **Engineer On-Call + Incident Team** |
|          | The **Engineer On-Call** is the initial owner of an incident, and this, is, in essence, the **Incident Team**. When the `EOC` escalates and incident to an IMOC, the IMOC takes ownership of the incident and can engage additional resources as necessary to augment the Incident Team. |
| `IMOC`   | **Incident Manager** |
|          | The **Incident Manager** is the tactical leader of the incident response team, and it must not be the person doing the technical work resolving the incident. The IMOC assembles the Incident Team, evaluates data (technical and otherwise) coming from team members, evaluates technical direction of incident resolution and coordinates troubleshooting efforts, and is responsible for documentation and debriefs after the incident.|
| `CMOC`   | **Communications Manager** |
|          | The **Communications Manager** is the communications leader of the incident response team. The focus of the Incident Team is on resolving the incident as quickly as possible. However, there is a critical need to disseminate information to appropriate stakeholders, including employees, eStaff, and end users. For `S1` (and possibly `S2`) incidents, this is a dedicated role. Otherwise, IMOC can handle communications. |

These definitions imply several on-call rotations for the different roles. 

The IMOC should be a technical person with a good understanding of GitLab.com's architecture. The CMOC is not required to be technical. The IMOC and the CMOC work in tandem to manage the incident resolution and timely communication.

## Ownership

The **initial and long-term owner** of an incident is the `EOC`, and as such, is responsible for incident declaration and its ultimate resolution. The `EOC` can temporarily cede ownership of an incident to an `IMOC`, but the `EOC` will still be responsible for producing the corresponding root-cause analysis (RCA).

## On-Call Runbooks

[On-Call runbooks](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/oncall.md) are available for engineers on-call.

## `S1` and `S2` Incidents

 `S1` and `S2` incidents are critical, and the  `EOC` can and should engage the `IMOC` .

Issue [`infra/5543`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5543) tracks automation for incident management.

All this steps can be done by slack using /create issue --initial severity , the internal steps are :
* Create an issue with the label `Incident`, on the `production` queue with the template for [Incident](https://gitlab.com/gitlab-com/gl-infra/production/issues/new?issuable_template=incident) . If it is not possible to generate the issue, start with the tracking document and create the incident issue later.
* Create an issue with the label `IncidentReview`  on the `infrastructure` queue with the template for [RCA](
https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/new?issuable_template=rca). If it is not possible to generate the issue, start with the tracking document and create the incident issue later.
* Ensure the initial severity label is accurate.
* Create and associate a google doc with the template RCA, to both issues, production and infrastructure. Populate and use the google doc as source of truth during the incidence. Mainly for real time multiple access.
* Contact the CMOC for support during the incident.

Following steps : 
* The owner of the issues is the on-call engineer / IMOC, will fill the issue of `production` and `infrastructure` when the incident is mitigated, with the info from the tracking document.
* When necessary, new tickets will be created with the label "Corrective Action" and linked with the RCA Issue on the infrastructure track.


## Communication

Information is a key asset during any incident. Properly managing the flow of information to its intended destination is critical in keeping interested stakeholders apprised of developments in a timely fashion. The awareness that an incident is in progress is critical in helping stakeholders plan for said changes.

This flow is determined by:

* the type of information,
* its intended audience,
* and timing sensitivity.

Furthermore, avoiding information overload is necessary to keep every stakeholder’s focus.

To that end, we will have:


* a dedicated incident bridge (zoom call) for all incidents.
* a dedicated `#incident-management` channel, since `#production` contains sizeable amounts of information and it takes effort to filter out non-relevant items. This is particularly important for the incident team, which must be focused on technical information to resolve the incident. While `#incident-management` is an open channel and anyone is free to join, we will encourage people to use other channels to communicate with the IMOC.
* periodic updates intended to the various audiences at place (CMOC handles this):
  * End-users (Twitter)
  * eStaff
  * Support staff
  * Employees at large
* [a dedicated repo for issues related to Production](https://gitlab.com/gitlab-com/production) separate from the queue that holds Infrastructures’s workload: namely, issues for incidents and changes.


