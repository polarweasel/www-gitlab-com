---
layout: markdown_page
title: "Evangelist Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

At GitLab our mission is to change all creative work from read-only to read-write so that **everyone can contribute**. In order to fulfill this mission, we need to create both the tools and platform to enable this change and a community of contributors who share our mission. We are just getting started in building the GitLab community and we invite you to join. 

There are many ways to participate in the GitLab community today: [contributing to an open source project](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/), [hosting your open source project on GitLab](https://about.gitlab.com/solutions/open-source/), or teaching your colleagues and collaborators about the value of [Concurrent DevOps](https://about.gitlab.com/concurrent-devops/). 

We are working on an evangelist program to support people who share our mission and want to give tech talks, run local meetups, or create videos or blogs. We will be announcing more in Q1 but for now, please email 'evangelists@gitlab.com' if you have feedback on our vision, ideas for how we can build our community, or suggestions for a name for our evangelist program. 

## Supporting community events

We'd love to support you if you are organizing an event, be it GitLab-centric or around a topic where GitLab content is relevant (e.g. DevOps meetup, hackathon, etc.). Depending on the number and type of attendees at an event, it may be owned by Corporate Marketing, Field Marketing, or Community Relations. Please review our [events decision tree](https://docs.google.com/spreadsheets/d/1aWsmsksPfOlX1t6TeqPkh5EQXergt7qjHAjGTxU27as/edit?usp=sharing) to ensure you are directing your event inquiry to the appropriate team.

You can get in touch with speakers from the GitLab team and the wider community to participate and do a talk at your event. Feel free to find a speaker on your region and contact them directly. Most speakers will also be able to do talks remotely if the event is virtual or if travel is a challenge.

[Find a GitLab speaker](https://about.gitlab.com/events/find-a-speaker/)

If you have questions, you can always reach us by sendind an e-mail to `evangelists@gitlab.com`.

## Organizing meetups

- We love and support meetups. If you would like someone from the GitLab team to stop by your event or are interested in having GitLab as a sponsor please email `evangelists@gitlab.com`. Please note, providing sufficient lead time (at least a month) will allow us to better support your event so feel free to reach out as soon as possible. 
- If you are interested in creating your own GitLab meetup or if you already have an existing meetup on meetup.com that you'd like linked to GitLab's meetup.com Pro account, please email `evangelists@gitlab.com`.  You can find the list of GitLab meetups on the [meetup.com page](https://www.meetup.com/pro/gitlab). 
- When you are getting started, we recommend scheduling at least 2 meetups. Publish your first meeting with a date and topic, and then a second meeting with a date and topic. The second meeting can have a flexible topic based on how the first meeting goes. The point of setting two meet-ups is to help build momentum in the new group.  
- Ideally, the first couple of meetups would include GitLab employees.  Once someone manages to have a couple of successful events, it'd be easy for a meet-up sustain itself. It is much harder to start new meet-ups versus maintaining existing ones. So we make an effort to support and keep existing events going.
- Reach out to other like-topic meetups and invite them to your meetup.
- Once you have scheduled your meetup, add the event to our [events page](/events/) so the whole world knows! Check out the [How to add an event to the events .yml](/handbook/marketing/corporate-marketing/#how-to-add-an-event-to-the-eventsyml/) section if you need help on how to add an event.
- If you purchase any food & beverage for the meetup event, we can help reimburse the expense.  A general guideline is $US 5/person for a maximum of $US 500 per each meetup. You will be asked to provide receipts, attendees list/photo, etc.  If you have questions or need help with food & beverage reimbursements, please email `evangelists@gitlab.com`.

## Becoming a tech speaker

We'll share more on this soon. For now please reach out to `evangelists@gitlab.com` if you're interested in giving a tech talk relating to GitLab. 

## Contributing content

We'll share more on this soon. For now please reach out to `evangelists@gitlab.com` if you're interested in contributing blog posts or videos to GitLab. 

### Blog

Stay tuned.
 
### Videos

Stay tuned.

## Helpful Resources

- [Merchandise](/handbook/marketing/community-relations/evangelist-program/workflows/merchandise.html)
- [Find a speaker](/handbook/marketing/community-relations/evangelist-program/workflows/find-a-speaker.html)

## Operations

- [Merchandise](/handbook/marketing/community-relations/evangelist-program/ops/merchandise.html)